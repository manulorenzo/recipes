//
//  PRPRecipe.h
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRPRecipe : NSObject <NSCoding>

@property(nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *directions;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSNumber *preparationTime;

@end
