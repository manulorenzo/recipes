//
//  PRPRecipesListViewController.h
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRPRecipesListDataSource.h"
#import "PRPRecipe.h"
#import "PRPRecipeEditorDelegate.h"

@interface PRPRecipesListViewController : UITableViewController <PRPRecipeEditorDelegate>
@property(nonatomic, strong) id <PRPRecipesListDataSource> dataSource;
-(void)finishedEditingRecipe:(PRPRecipe *)recipe;
@end
