//
//  PRPViewController.h
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRPRecipe.h"

@interface PRPViewController : UIViewController

@property (nonatomic, strong) PRPRecipe *recipe;

@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UITextView *directionsView;

@property (nonatomic, strong) IBOutlet UILabel *prepTime;
@property (nonatomic, strong) NSNumberFormatter *formatter;

@end