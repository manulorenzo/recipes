//
//  PRPRecipeEditorViewController.h
//  Recipes2
//
//  Created by manu on 12/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRPRecipesListViewController.h"

@class PRPRecipe;

@interface PRPRecipeEditorViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate>

@property(nonatomic, strong) PRPRecipe *recipe;

@property(nonatomic, strong) NSNumberFormatter *formatter;
@property(nonatomic, strong) IBOutlet UITextField *titleField;
@property(nonatomic, strong) IBOutlet UITextView *directionsText;
@property(nonatomic, strong) IBOutlet UILabel *prepTimeLabel;
@property(nonatomic, strong) IBOutlet UIImageView *recipeImage;
@property(nonatomic, strong) IBOutlet UIStepper *prepTimeStepper;

@property(nonatomic, assign) id<PRPRecipeEditorDelegate> delegate;

-(IBAction)changePreparationTime:(UIStepper *)sender;
-(IBAction)done:(UIBarButtonItem *)sender;
@end
