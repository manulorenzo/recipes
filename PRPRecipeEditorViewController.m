//
//  PRPRecipeEditorViewController.m
//  Recipes2
//
//  Created by manu on 12/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "PRPRecipeEditorViewController.h"
#import "PRPRecipe.h"

@interface PRPRecipeEditorViewController ()

@end

@implementation PRPRecipeEditorViewController

-(IBAction)changePreparationTime:(UIStepper *)sender {
    NSInteger value = (NSInteger) [sender value];
    self.recipe.preparationTime = [NSNumber numberWithInteger:value];
    self.prepTimeLabel.text = [self.formatter stringFromNumber:self.recipe.preparationTime];
}

-(IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate finishedEditingRecipe:self.recipe];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.titleField.text = self.recipe.title;
    self.directionsText.text = self.recipe.directions;
    self.prepTimeLabel.text = [self.formatter stringFromNumber:self.recipe.preparationTime];
    self.prepTimeStepper.value = [self.recipe.preparationTime doubleValue];
    if (nil != self.recipe.image) {
        self.recipeImage.image = self.recipe.image;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([@"editDirections" isEqualToString:segue.identifier]) {
        [[segue destinationViewController] setRecipe:self.recipe];
    }
    
    if ([@"choosePhoto" isEqualToString:segue.identifier]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.formatter = [[NSNumberFormatter alloc]init];
}

# pragma mark - UITextFieldDelagate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.recipe.title = textField.text;
    [self.delegate recipeChanged:self.recipe];
}

# pragma mark - UIImagePickerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.recipe.image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
