//
//  PRPRecipesListDataSource.h
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRPRecipe.h"

@class PRPRecipe;
@protocol PRPRecipesListDataSource <NSObject>

-(NSInteger)recipeCount;

-(PRPRecipe *)recipeAtIndex:(NSInteger)index;

-(void)deleteRecipeAtIndex:(NSInteger)index;

-(PRPRecipe *)createNewRecipe;

-(NSUInteger)indexOfRecipe:(PRPRecipe *)recipe;

-(void)recipesChanged;

@end
