//
//  PRPViewController.m
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "PRPViewController.h"

@interface PRPViewController ()

@end

@implementation PRPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.formatter = [[NSNumberFormatter alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = self.recipe.title;
    NSLog(@"texto: %@", self.recipe.directions);
    self.directionsView.text = self.recipe.directions;
    if (self.recipe.image != nil) {
        self.imageView.image = self.recipe.image;
    }
    self.prepTime.text = [self.formatter stringFromNumber:self.recipe.preparationTime];
}

@end
