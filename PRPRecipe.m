//
//  PRPRecipe.m
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "PRPRecipe.h"

@implementation PRPRecipe

-(id)init {
    self = [super init];
    if (self) {
        self.title = @"New recipe";
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.directions forKey:@"directions"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.preparationTime forKey:@"preparationTime"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.directions = [aDecoder decodeObjectForKey:@"directions"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
        self.preparationTime = [aDecoder decodeObjectForKey:@"preparationTime"];
    }
    return self;
}

@end
