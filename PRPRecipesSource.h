//
//  PRPRecipesSource.h
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRPRecipesListDataSource.h"

@interface PRPRecipesSource : NSObject <PRPRecipesListDataSource>
@end
