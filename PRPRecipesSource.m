//
//  PRPRecipesSource.m
//  Recipes
//
//  Created by manu on 08/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "PRPRecipesSource.h"
#import "PRPRecipesListDataSource.h"

@interface PRPRecipesSource()
@property (nonatomic, strong) NSMutableArray *recipes;
@end

@implementation PRPRecipesSource

- (NSArray *) recipes {
    if (!_recipes) {
        NSMutableArray *localRecipes = [NSMutableArray array];
        PRPRecipe *recipe = [[PRPRecipe alloc] init];
        recipe.directions = @"0 - Put some stuff in, and the other stuff, then stir";
        recipe.title = @"0 - One Fine Food";
        recipe.preparationTime = [NSNumber numberWithInt:30];
        [localRecipes addObject:recipe];
        recipe = [[PRPRecipe alloc]init];
        recipe.directions = @"1 - Put some stuff in, and the other stuff, then stir";
        recipe.title = @"1 - One Fine Food";
        recipe.preparationTime = [NSNumber numberWithInt:45];
        [localRecipes addObject:recipe];
        self.recipes = localRecipes;
    }
    return _recipes;
}

-(PRPRecipe *)createNewRecipe {
    PRPRecipe *recipe = [[PRPRecipe alloc]init];
    [self.recipes addObject:recipe];
    return recipe;
}

-(NSInteger)recipeCount {
    return [self.recipes count];
}

-(PRPRecipe *)recipeAtIndex:(NSInteger)index {
    return [self.recipes objectAtIndex:index];
}

-(void)deleteRecipeAtIndex:(NSInteger)index {
    [self.recipes removeObjectAtIndex:index];
}

-(NSUInteger)indexOfRecipe:(PRPRecipe *)recipe {
    return [self.recipes indexOfObject:recipe];
}

@end