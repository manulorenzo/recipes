//
//  PRPRecipeEditorDelegate.h
//  Recipes2
//
//  Created by manu on 28/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRPRecipe;

@protocol PRPRecipeEditorDelegate <NSObject>

- (void)finishedEditingRecipe:(PRPRecipe *)recipe;
- (void)recipeChanged:(PRPRecipe *)recipe;

@end