//
//  PRPRecipesDocument.m
//  Recipes2
//
//  Created by manu on 16/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import "PRPRecipesDocument.h"

@interface PRPRecipesDocument()

@property (nonatomic, strong) NSMutableArray *recipes;

@end

@implementation PRPRecipesDocument

- (NSMutableArray *)recipes {
    if (!_recipes) {
        self.recipes = [NSMutableArray array];
    }
    return _recipes;
}

- (void)recipesChanged {
    [self updateChangeCount:UIDocumentChangeDone];
}

-(NSInteger)recipeCount {
    return [self.recipes count];
}

-(PRPRecipe *)recipeAtIndex:(NSInteger)index {
    return [self.recipes objectAtIndex:index];
}

-(void)deleteRecipeAtIndex:(NSInteger)index {
    [self.recipes removeObjectAtIndex:index];
    [self updateChangeCount:UIDocumentChangeDone];
}

-(PRPRecipe *)createNewRecipe {
    PRPRecipe *recipe = [[PRPRecipe alloc] init];
    [self.recipes addObject:recipe];
    [self updateChangeCount:UIDocumentChangeDone];
    return recipe;
}

-(NSUInteger)indexOfRecipe:(PRPRecipe *)recipe {
    return [self.recipes indexOfObject:recipe];
}

-(id)contentsForType:(NSString *)typeName error:(NSError **)outError {
    return [NSKeyedArchiver archivedDataWithRootObject:self.recipes];
}

-(BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError **)outError {
    BOOL success = NO;
    if ([contents isKindOfClass:[NSData class]] && [contents length] >0) {
        NSData *data = (NSData *)contents;
        self.recipes = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        success = YES;
    }
    return success;
}

- (id)initWithFileURL:(NSURL *)url { self = [super initWithFileURL:url]; if (self) {
    id value = nil;
    NSError *error = nil;
    if(![url getResourceValue:&value forKey:NSURLAttributeModificationDateKey
                        error:&error]) {
        [self saveToURL:url forSaveOperation:UIDocumentSaveForCreating
      completionHandler:^(BOOL success) {
          if(!success) {
              NSLog(@"Failed to create file");
          }
      }];
    }
}
    return self;
}

- (void)handleError:(NSError *)error userInteractionPermitted:(BOOL)userInteractionPermitted {
    if([[error domain] isEqualToString:NSCocoaErrorDomain] && [error code] == NSFileReadNoSuchFileError) {
        [self saveToURL:[self fileURL]
       forSaveOperation:UIDocumentSaveForCreating
      completionHandler:^(BOOL success) {
          // ignore it here, we just wanted to make sure the document is created
          NSLog(@"handled open error with a new doc"); }];
    } else {
        // if it's not a NSFileReadNoSuchFileError just call super
        [super handleError:error userInteractionPermitted:userInteractionPermitted];
    }
}

@end
