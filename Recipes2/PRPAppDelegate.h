//
//  PRPAppDelegate.h
//  Recipes2
//
//  Created by manu on 10/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
