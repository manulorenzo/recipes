//
//  PRPRecipesDocument.h
//  Recipes2
//
//  Created by manu on 16/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRPRecipesListDataSource.h"

@interface PRPRecipesDocument : UIDocument <PRPRecipesListDataSource>

@end
