//
//  main.m
//  Recipes2
//
//  Created by manu on 10/01/2013.
//  Copyright (c) 2013 Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PRPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PRPAppDelegate class]));
    }
}
